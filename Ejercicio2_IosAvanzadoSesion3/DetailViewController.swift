//
//  DetailViewController.swift
//  Ejercicio2_IosAvanzadoSesion3
//
//  Created by Fran on 17/11/17.
//  Copyright © 2017 Fran. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UIPopoverPresentationControllerDelegate, TableViewControllerDelegate {
    var tableView : TableViewController!
    
    func opcionSeleccionada(nombreOpcion: String) {
        // Comprobamos que se recibe aquí la opción seleccionada. Aquí ya podríamos hacer lo que queramos con el valor recibido.

        print("Seleccionado \(nombreOpcion)")
        
        // Ocultamos el popover
        self.dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var boton: UIBarButtonItem!
    @IBAction func accionOpciones(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "tablaStoryboard") as! TableViewController
        
        controller.delegate = self
        controller.preferredContentSize = CGSize(width: 250, height: 135)
        
        
        controller.opciones = (self.detailItem?.arrayActores)!
        // Presentamos el controlador
        // - en iPad, será un Popover
        // - en iPhone, será un action sheet
        controller.modalPresentationStyle = UIModalPresentationStyle.popover
        
        self.present(controller, animated: true, completion: nil)
        
        // Configuramos el popover (sí, después de presentarlo, es tan ilógico como parece)
        let popController = controller.popoverPresentationController
        
        // Todo popover debe tener un botón de referencia o una posición en la pantalla desde la que sale (con barButtonItem o con sourceView):
        popController?.barButtonItem = self.navigationItem.rightBarButtonItem
        popController?.permittedArrowDirections = UIPopoverArrowDirection.any
        popController?.delegate = self
        
    }
    
    
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var fecha: UILabel!
    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var imagen: UIImageView!
    
    @IBOutlet weak var tituloBarraVista: UINavigationItem!
    @IBOutlet weak var descripcion: UITextView!
    override func viewWillLayoutSubviews() {
        
        if view.bounds.size.width >= view.bounds.size.height {
            self.stackView.axis = .horizontal
        }
        else {
            self.stackView.axis = .vertical
        }
    }
    func configureView() {
        // Update the user interface for the detail item.
        if let detail = self.detailItem {
            if let label = self.titulo {
                label.text = detail.titulo
            }
            if let imagen = self.imagen {
                imagen.image = UIImage(named : detail.caratula)
            }
            if let descripcion = self.descripcion {
                descripcion.text = detail.descripcion!
            }
            if let fecha = self.fecha{
                fecha.text = detail.fecha
            }
            if let tituloBarraVista = self.tituloBarraVista{
                tituloBarraVista.title = detail.titulo + " " + detail.fecha
            }
           
            
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // 1. request an UITraitCollection instance
        

        
        let deviceIdiom = UIScreen.main.traitCollection.userInterfaceIdiom
        
        // 2. check the idiom
        switch (deviceIdiom) {
            
        case .pad:
            print("iPad style UI")
            boton.isEnabled = true
        case .phone:
            print("iPhone and iPod touch style UI")
            boton.isEnabled = false
        case .tv:
            print("tvOS style UI")
            boton.isEnabled = false
        default:
            print("Unspecified UI idiom")
            boton.isEnabled = false
        }
        configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    var detailItem: Pelicula? {
        didSet {
            // Update the view.
            configureView()
        }
    }


}

