//
//  Pelicula.swift
//  Ejercicio2_IosAvanzadoSesion3
//
//  Created by Fran on 17/11/17.
//  Copyright © 2017 Fran. All rights reserved.
//

import Foundation
class Pelicula {
    var titulo : String
    var caratula : String
    var fecha : String
    var descripcion : String?
   
    var arrayActores = [String]()
    init(titulo: String, caratula: String, fecha: String, descripcion: String?,arrayActores:[String] ) {
        self.titulo = titulo
        self.fecha = fecha
        self.caratula = caratula
        self.descripcion = descripcion
        self.arrayActores = arrayActores
    }
}

